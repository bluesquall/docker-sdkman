FROM debian:bullseye-slim
MAINTAINER https://bitbucket.org/bluesquall/docker-sdkman/issues

RUN apt-get update \
 && apt-get install --yes --no-install-recommends \
      curl \
      ca-certificates \
      sudo \
      unzip \
      zip \
      zsh \
 && apt-get autoremove --yes && apt-get clean

ARG USERNAME=artoo
ARG USER_UID=18242
ARG USER_GID=${USER_UID}

RUN groupadd --gid ${USER_GID} ${USERNAME} \
 && useradd --uid ${USER_UID} --gid ${USER_GID} --shell /bin/zsh --create-home ${USERNAME} \
 && echo ${USERNAME} ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/${USERNAME} \
 && chmod 0440 /etc/sudoers.d/${USERNAME}

USER ${USERNAME}
ENV HOME /home/${USERNAME}
WORKDIR ${HOME}

ENV SDKMAN_HOME ${HOME}/.sdkman
RUN curl -S https://get.sdkman.io | bash \
 && sed -i "s/sdkman_auto_answer=false/sdkman_auto_answer=true/g" ${SDKMAN_HOME}/etc/config \
 && /bin/bash -c "source ${SDKMAN_HOME}/bin/sdkman-init.sh"

